import React from 'react';
import { shallow } from 'enzyme';
import ExpenseListItem from '../../components/ExpenseListItem';
import expenses from '../fixtures/expenses';

test('should render ExpanseListItem correctly', () => {
    // This can be used instead of '...expenses[0]'
    // const expense = {
    //     id: expenses[0].id,
    //     description: expenses[0].description,
    //     amount: expenses[0].amount,
    //     createdAt: expenses[0].createdAt
    // };
    const wrapper = shallow(<ExpenseListItem {...expenses[0]}/>);
    expect(wrapper).toMatchSnapshot();
});